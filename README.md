# Problemas de querys en MongoDB

* ¿Película del año 2013 que está clasificada (rated) PG-13 y no ganó premios?

Respuesta:
```javascript
db.movieDetails.find({ $or: [{'awards.wins': 0}, {'awards.wins': {$exists: false }}], 'rated': 'PG-13', 'year': 2013}, { 'title': 1 }).pretty()
```
* ¿Cuántas películas listan "Sweden" como segunda en la lista de countries?

Respuesta:
```javascript
db.movieDetails.find({ 'countries.1': 'Sweden'}).count();
```

* ¿Cuántos documentos en la colección movieDetails tienen solamente los siguientes géneros: "Comedy" y "Crime" con "Comedy" al principio?

Respuesta:
```javascript
db.movieDetails.find({'genres': ['Comedy', 'Crime']}).count();
```
* ¿Cuántos documentos en la colección movieDetails contienen los geńeros: "Comedy" y "Crime"?

Respuesta:
```javascript
db.movieDetails.find({'genres': {$all: ['Comedy', 'Crime']}}).count();
```
* ¿Cuántas películas ha realizado el actor "Will Smith" como personaje principal?

Respuesta:
```javascript
db.movieDetails.find({'actors.0': 'Will Smith'}).count();
```
* ¿Cuántas películas ha realizado el actor "James Doohan" como personaje secundario?

Respuesta:
```javascript
db.movieDetails.find({'actors.0': 'James Doohan'}).count();
```
* Supóngase que nuestros documentos en vez de estar estructurados de la siguiente forma:

```json
"awards" : {
    "wins" : 56,
    "nominations" : 86,
    "text" : "Won 2 Oscars. Another 56 wins and 86 nominations."
}
```

Están estructurados de la siguiente:

```json
"awards" : {
    "oscars" : [
        {"award": "bestAnimatedFeature", "result": "won"},
        {"award": "bestMusic", "result": "won"},
        {"award": "bestPicture", "result": "nominated"},
        {"award": "bestSoundEditing", "result": "nominated"},
        {"award": "bestScreenplay", "result": "nominated"}
    ],
    "wins" : 56,
    "nominations" : 86,
    "text" : "Won 2 Oscars. Another 56 wins and 86 nominations."
}
```
¿Que query tienes que ejecutar en la shell de Mongo para que te regrese todas las películas en movieDetails que han ganado o fueron 
nominadas al Oscar como mejor película?
